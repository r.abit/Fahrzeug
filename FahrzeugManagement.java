import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

/**
 * @author Rabit Ljatifi
 * Matrikelnummer: 1147409
 */

public class FahrzeugManagement {
	
	private FahrzeugDAO fahrzeugDAO;
	
	public FahrzeugManagement(FahrzeugDAO fahrzeugDAO){
		this.fahrzeugDAO = fahrzeugDAO;
	}
	
	public void addFahrzeug(Fahrzeug fahrzeug) {
        this.fahrzeugDAO.speichereFahrzeug(fahrzeug);
    }

	public void fahrzeugList() {
		
		ArrayList<Fahrzeug> fahrzeuge = this.fahrzeugDAO.getFahrzeugList();

		for(Fahrzeug gen : fahrzeuge) {
		    System.out.println(gen.toString());
		}
		System.out.println("aksjdlks");
	}

	public Fahrzeug showId(int showID) {
		return (fahrzeugDAO.getFahrzeugbyId(showID));
	}
	
	public void removeFahrzeug(int id) {
		ArrayList<Fahrzeug> fahrzeuge = this.fahrzeugDAO.getFahrzeugList();
		
		showId(id);
        
		for(Fahrzeug fahrzeug: fahrzeuge)
        		if( fahrzeug.getId() == id )
        			this.fahrzeugDAO.loescheFahrzeug(id);
		
	}
	
	public void checkId(int checkId) {
		
		ArrayList<Fahrzeug> fahrzeuge = this.fahrzeugDAO.getFahrzeugList();
		
		for(Fahrzeug gen : fahrzeuge) {
			if (gen.getId() == checkId)
				throw new IllegalArgumentException("Error: Fahrzeug bereits vorhanden. (id=" + checkId + ")");
		}
	}
	
	public int count() {
		
		ArrayList<Fahrzeug> fahrzeuge = this.fahrzeugDAO.getFahrzeugList();
		int count = 0;
		
		for(@SuppressWarnings("unused") Fahrzeug gen : fahrzeuge)
			++count;
		
		return count;
	}

	public int countPkw() {
		
		ArrayList<Fahrzeug> fahrzeuge = this.fahrzeugDAO.getFahrzeugList();
		int count = 0;
		
		for(Fahrzeug fahrzeug: fahrzeuge)
            if (fahrzeug instanceof Pkw)
                count++;
		
		return count;
	}

	public int countLkw() {
		
		ArrayList<Fahrzeug> fahrzeuge = this.fahrzeugDAO.getFahrzeugList();
		int count = 0;
		
		for(Fahrzeug fahrzeug: fahrzeuge)
            if (fahrzeug instanceof Lkw)
                count++;
		
		return count;
	}

	public String avgPreis() {
		ArrayList<Fahrzeug> fahrzeuge = this.fahrzeugDAO.getFahrzeugList();
		double avgPreis = 0;
		int count = 0;
		
		for(Fahrzeug fahrzeug: fahrzeuge) {
			avgPreis+= fahrzeug.getPreis();
			count++;
		}
		
		return getDecimalFormat().format((avgPreis/count));
	}

	public static DecimalFormat getDecimalFormat() {
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
		dfs.setDecimalSeparator('.');
		return new DecimalFormat("0.00", dfs);
	}

	public void oldestFahrzeug() {
		
		ArrayList<Fahrzeug> fahrzeuge = this.fahrzeugDAO.getFahrzeugList();
		
		int oldest = 0;
		
		for(Fahrzeug fahrzeug: fahrzeuge) {
			for(Fahrzeug gen: fahrzeuge) 
				if (gen.getAlter() >= oldest) 
					oldest = gen.getAlter();
			if (fahrzeug.getAlter() == oldest)
				System.out.println("Id: " + fahrzeug.getId());
		}
	}
	
}