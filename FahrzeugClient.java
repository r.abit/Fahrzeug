/**
 * @author Rabit Ljatifi
 * Matrikelnummer: 1147409
 */

public class FahrzeugClient {

	public static void main(String[] args) {	
		
		FahrzeugManagement man = new FahrzeugManagement(new SerializedFahrzeugDAO(args[0]));
		
		try {
			if (args[1].equals("add")) {
				if(args[2].equals("pkw")) {
					Fahrzeug pkw1 = new Pkw(args[4], args[5], Integer.parseInt(args[6]), Double.parseDouble(args[7]), Integer.parseInt(args[3]), Integer.parseInt(args[8]));
					man.checkId(Integer.parseInt(args[3]));
					man.addFahrzeug(pkw1);
				}
				else if (args[2].equals("lkw")) {
					Fahrzeug lkw = new Lkw(args[4], args[5], Integer.parseInt(args[6]), Double.parseDouble(args[7]), Integer.parseInt(args[3]));
					man.checkId(Integer.parseInt(args[3]));
					man.addFahrzeug(lkw);
				}
				else
					throw new IllegalArgumentException("Error: Parameter ungueltig.");
			}
			
			else if (args[1].equals("show")) {
				if (args.length == 2)
					man.fahrzeugList();
				else if (args.length == 3)
					System.out.println(man.showId(Integer.parseInt(args[2])));
				
				else
					throw new IllegalArgumentException("Error: Parameter ungueltig.");
			}
			
			else if (args[1].equals("del") && (args.length == 3))
				man.removeFahrzeug(Integer.parseInt(args[2]));
			
			else if (args[1].equals("count")) {
				if (args.length == 2)
					System.out.println(man.count());
				else if (args.length == 3) {
					if (args[2].equals("pkw"))
						System.out.println(man.countPkw());
					else if (args[2].equals("lkw"))
						System.out.println(man.countLkw());
					else
						throw new IllegalArgumentException("Error: Parameter ungueltig.");
				}
				
				else
					throw new IllegalArgumentException("Error: Parameter ungueltig.");
			}
			
			else if (args[1].equals("meanprice")) {
				System.out.println(man.avgPreis());
			}
			
			else if (args[1].equals("oldest")) {
				man.oldestFahrzeug();
			}
			
			else
				throw new IllegalArgumentException("Error: Parameter ungueltig.");
				
		}
		catch(NumberFormatException e) {
			System.out.println("Error: Parameter ungueltig.");
		}
		catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		
		catch(Exception e) {
			System.out.println("Error: Parameter ungueltig.");
    		}
		
		
	}

}