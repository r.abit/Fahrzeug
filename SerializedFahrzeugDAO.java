import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * @author Rabit Ljatifi
 * Matrikelnummer: 1147409
 */

public class SerializedFahrzeugDAO implements FahrzeugDAO {

	private String filename;

    public SerializedFahrzeugDAO(String filename){
        this.filename = filename;
    }

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Fahrzeug> getFahrzeugList() {
		
		File file = new File(filename);
		
		ArrayList<Fahrzeug> fahrzeuge = new ArrayList<Fahrzeug>();
		if (!file.exists()) return null;
		
        try (FileInputStream fis = new FileInputStream(filename);) {
            ObjectInputStream ois = new ObjectInputStream(fis);
            fahrzeuge = (ArrayList<Fahrzeug>) ois.readObject();
            ois.close();
        }
        catch(Exception e) {
        		System.err.println("Fehler bei Deserialisierung: " + e.getMessage());
        		System.exit(1);
        }

        return fahrzeuge;

	}

	@Override
	public Fahrzeug getFahrzeugbyId(int id) {
		ArrayList<Fahrzeug> fahrzeuge = this.getFahrzeugList();
		
        for(Fahrzeug fahrzeug: fahrzeuge)
        		if( fahrzeug.getId() == id )
                return fahrzeug;
        
		throw new IllegalArgumentException("Error: Fahrzeug nicht vorhanden. (id=" + id +")");
	}

	@Override
    public void speichereFahrzeug(Fahrzeug fahrzeug) {
        ArrayList<Fahrzeug> farzeuge = this.getFahrzeugList();
        farzeuge.add(fahrzeug);
        this.saveFahrzeugState(farzeuge);
    }

    @Override
    public void loescheFahrzeug(int id) {
    	
    		ArrayList<Fahrzeug> fahrzeuge = this.getFahrzeugList();
		
        for(Fahrzeug fahrzeug: fahrzeuge)
        		if( fahrzeug.getId() == id ) {
        			fahrzeuge.remove(fahrzeug);
        			this.saveFahrzeugState(fahrzeuge);
        			break;
        		}
        
	}

    private void saveFahrzeugState(ArrayList<Fahrzeug> fahrzeuge) {
        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(fahrzeuge);
            oos.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
	
}
