/**
 * @author Rabit Ljatifi
 * Matrikelnummer: 1147409
 */

public class Lkw extends Fahrzeug {

	private static final long serialVersionUID = 1L;
	
	public Lkw(String marke, String modell,int baujahr, double grundpreis, int id) {
		this.setMarke(marke);
		this.setModell(modell);
		this.setBaujahr(baujahr);
		this.setGrundpreis(grundpreis);
		this.setId(id);
	}

	@Override
	public double getRabatt() {
		
		int prozent = getAlter()* 5;
		
		if(prozent > 20)
			prozent = 20;
		
		return prozent;
	}
	
	@Override
	public String toString(){
		String out ="";
        out += "Typ:		LKW\n";
        out += "Id:		" + this.getId() + "\n";
        out += "Marke:		" + this.getMarke() + "\n";
        out += "Modell:		" + this.getModell() + "\n";
        out += "Baujahr:	" + this.getBaujahr() + "\n";
        out += "Grundpreis:	" + getDecimalFormat().format(this.getGrundpreis()) + "\n";
        out += "Preis:		" + getDecimalFormat().format((this.getPreis())) + "\n";
        return out;
	}

	
}
