/**
 * @author Rabit Ljatifi
 * Matrikelnummer: 1147409
 */

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;


public abstract class Fahrzeug implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String marke;
	private String modell;
	private int baujahr;
	private double grundpreis;
	private int id;
	
	public abstract double getRabatt();
	
	
	public Fahrzeug() {
	}
	
	public Fahrzeug(String marke, String modell,int baujahr, double grundpreis, int id) {
		this.setMarke(marke);
		this.setModell(modell);
		this.setBaujahr(baujahr);
		this.setGrundpreis(grundpreis);
		this.setId(id);
	}

	
	public String getMarke() {
		return marke;
	}

	public void setMarke(String marke) {
		this.marke = marke;
	}

	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	public int getBaujahr() {
		return baujahr;
	}

	public void setBaujahr(int baujahr) {
		if (baujahr > currentYear()) {
			throw new IllegalArgumentException("Error: Baujahr ungueltig.");
		}
		else
			this.baujahr = baujahr;
	}

	public double getGrundpreis() {
		return grundpreis;
	}

	public void setGrundpreis(double preis) {
		if (preis <= 0)
			throw new IllegalArgumentException("Error: Grundpreis ungueltig.");
	    this.grundpreis = preis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getAlter() {
		
		return currentYear() - this.getBaujahr();
	}
	
	public double getPreis() {
		return (this.getGrundpreis()*(100 - this.getRabatt())) / 100;
	}
	
	public int currentYear() {
		Calendar currentYear = Calendar.getInstance();
		return currentYear.get(Calendar.YEAR);
	}

	public String toString() {
		return "toString() in Pkw oder Lkw falsch!";
	}
	
	
	public static DecimalFormat getDecimalFormat() {
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
		dfs.setDecimalSeparator('.');
		return new DecimalFormat("0.00", dfs);
	}		
}
