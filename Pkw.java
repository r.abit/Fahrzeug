/**
 * @author Rabit Ljatifi
 * Matrikelnummer: 1147409
 */

public class Pkw extends Fahrzeug {
	
	private static final long serialVersionUID = 1L;
	
	private int servicejahr;
	
	public int getServicejahr() {
		return servicejahr;
	}

	public void setServicejahr(int servicejahr) {
		if((servicejahr > currentYear()) || (servicejahr < getBaujahr()))
			throw new IllegalArgumentException("Error: Servicejahr ungueltig.");
		else
			this.servicejahr = servicejahr;
	}
	
	public Pkw(String marke, String modell,int baujahr, double grundpreis, int id, int servicejahr) {
		this.setMarke(marke);
		this.setModell(modell);
		this.setBaujahr(baujahr);
		this.setGrundpreis(grundpreis);
		this.setId(id);
		this.setServicejahr(servicejahr);
	}

	@Override
	public double getRabatt() {
		
		int prozent = getAlter()* 5;
		
		int serviceRabatt = currentYear() - servicejahr;
		serviceRabatt = serviceRabatt * 2;
		
		if(prozent+serviceRabatt > 15)
			prozent = 15;
		else
			prozent = prozent+serviceRabatt;
		
		return prozent;
	}
	
	@Override
	public String toString(){
		String out ="";
        out += "Typ:		PKW\n";
        out += "Id:		" + this.getId() + "\n";
        out += "Marke:		" + this.getMarke() + "\n";
        out += "Modell:		" + this.getModell() + "\n";
        out += "Baujahr:	" + this.getBaujahr() + "\n";
        out += "Grundpreis:	" + getDecimalFormat().format(this.getGrundpreis()) + "\n";
        out += "Servicejahr:	" + this.getServicejahr() + "\n";
        out += "Preis:		" + getDecimalFormat().format((this.getPreis())) + "\n";
        return out;
		
	}
}
