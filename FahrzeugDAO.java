/**
 * @author Rabit Ljatifi
 * Matrikelnummer: 1147409
 */

import java.util.ArrayList;

public interface FahrzeugDAO {

	public ArrayList<Fahrzeug> getFahrzeugList();

	public Fahrzeug getFahrzeugbyId(int id);
	
	public void speichereFahrzeug(Fahrzeug fahrzeug);

	public void loescheFahrzeug(int id);
}
